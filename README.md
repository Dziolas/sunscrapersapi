# ECB currency exchange API
Small project for harvesting currency exchange values from European Central Bank.
## Data harvesting
Data harveting is performed by Scrapy spider. Harvested data is stored in PostgersDB using custom pipeline. To start harvesting go to project folder and run `scrapy crawl ecbspider`.
## Starting HTTP server
Project is build with Flask microframerwok which comes with a simple HTTP server. To start the server run `flask run` command. Remember to set the environment variable FLASK_APP pointing to project catalogue.
## Required environment variables
```
FLASK_APP = /path/to/project/folder/;
POSTGRES_URL = localhost;
POSTGRES_DB = dbname;
POSTGRES_USER = dbuser;
POSTGRES_PORT = 5432;
POSTGRES_PASSWORD = dbuserpassword;
```
## Starting tests
Navigate to `/sunscrapersapi/tests` and run `pytest`.
## Future development
- Additional tests for API
- Tests for Spider
- Scheduling spider to run automaticaly with Celery
