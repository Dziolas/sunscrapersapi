import json
import pytest
from sqlalchemy_utils import database_exists, create_database, drop_database
from sunscrapersapi import create_app
from sunscrapersapi.models import *


@pytest.fixture(scope='module')
def test_client():
    flask_app = create_app('test')
    ctx = flask_app.app_context()
    ctx.push()
    testing_client = flask_app.test_client()
    init_db(db)
    load_demo_data(db)
    yield testing_client
    drop_db(db)
    ctx.pop()


def init_db(db):
    if database_exists(db.engine.url):
        print('Deleting test database.')
        drop_database(db.engine.url)
    if not database_exists(db.engine.url):
        print('Creating test database.')
        create_database(db.engine.url)
    print('Creating tables.')
    db.create_all()


def drop_db(db):
    print('Deleting test database.')
    drop_database(db.engine.url)


def load_demo_data(db):
    with open("./demo_data.json") as f:
        j = json.loads(f.read())
        for ex in j['exchanges']:
            print("Inserting data: {}".format(ex))
            base = Currency.query.filter(Currency.name == ex['base_currency']).first()
            if not base:
                base = Currency(name=ex['base_currency'])
                db.session.add(base)
            target = Currency.query.filter(Currency.name == ex['target_currency']).first()
            if not target:
                target = Currency(name=ex['target_currency'])
                db.session.add(target)
            if not Fx.query. \
                    filter(Fx.base_currency == base,
                           Fx.target_currency == target,
                           Fx.date == ex['date']). \
                    first():
                del (ex['target_currency'])
                del (ex['base_currency'])
                new_fx = Fx(**ex)
                new_fx.base_currency = base
                new_fx.target_currency = target
                db.session.add(new_fx)
            else:
                print('Entry already exist.')
        db.session.commit()


@pytest.fixture(scope='session')
def demo_data():
    with open("./demo_data.json") as f:
        return json.loads(f.read())
