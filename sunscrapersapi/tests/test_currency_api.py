import json

class TestCurrencyApi:
    def test_get(self, test_client):
        resp = test_client.get('/currency/list')
        assert resp.status_code == 200

    def test_check_number_of_currencies(self, test_client, demo_data):
        currencies = []
        for ex in demo_data['exchanges']:
            if ex['base_currency'] not in currencies:
                currencies.append(ex['base_currency'])
            if ex['target_currency'] not in currencies:
                currencies.append(ex['target_currency'])

        resp = test_client.get('/currency/list')
        data = json.loads(resp.data)
        assert data['total'] == len(currencies)
