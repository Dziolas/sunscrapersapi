from flask_restful import Api
from sunscrapersapi.resources.fx import FX, FXAll
from sunscrapersapi.resources.currency import Currency

api = Api()
api.add_resource(FX, '/fx/<string:base>/<string:target>')
api.add_resource(FXAll, '/fx/<string:base>')
api.add_resource(Currency, '/currency/list')
