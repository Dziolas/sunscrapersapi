import os
from flask import Flask


def get_env_variable(name):
    try:
        return os.environ[name]
    except KeyError:
        message = "Expected environment variable '{}' not set.".format(name)
        raise Exception(message)

POSTGRES_URL = get_env_variable("POSTGRES_URL")
POSTGRES_USER = get_env_variable("POSTGRES_USER")
POSTGRES_PASSWORD = get_env_variable("POSTGRES_PASSWORD")
POSTGRES_PORT = get_env_variable("POSTGRES_PORT")
POSTGRES_DB = get_env_variable("POSTGRES_DB")

DB_URL = 'postgresql+psycopg2://{user}:{pw}@{url}:{port}/{db}'.format(user=POSTGRES_USER,pw=POSTGRES_PASSWORD,url=POSTGRES_URL,port=POSTGRES_PORT,db=POSTGRES_DB)


def create_app(env='dev'):
    app = Flask(__name__)
    if env == 'dev':
        app.config.from_pyfile('config.py')
        app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
    elif env == 'test':
        app.config.from_pyfile('config_test.py')

    app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {'connect_args': {"options": "-c timezone=utc"}}

    from sunscrapersapi.api import api
    api.init_app(app)

    from sunscrapersapi.models import db
    db.init_app(app)

    @app.route("/")
    def hello():
        return "Hello World!"

    return app
