from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.types import Float

db = SQLAlchemy()


class Currency(db.Model):
    __tablename__ = 'currency'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)

    bases = relationship("Fx", back_populates="base_currency", foreign_keys="[Fx.base_currency_id]")
    targets = relationship("Fx", back_populates="target_currency", foreign_keys="[Fx.target_currency_id]")


class Fx(db.Model):
    __tablename__ = 'exchange'

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime(timezone=True), nullable=False)
    base_currency_id = db.Column(db.Integer, ForeignKey('currency.id'), nullable=False)
    target_currency_id = db.Column(db.Integer, ForeignKey('currency.id'), nullable=False)
    value = db.Column(Float, nullable=False)

    base_currency = relationship("Currency", back_populates="bases", foreign_keys=[base_currency_id])
    target_currency = relationship("Currency", back_populates="targets", foreign_keys=[target_currency_id])
