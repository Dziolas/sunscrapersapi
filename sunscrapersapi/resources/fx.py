from flask import current_app, request
from flask_restful import reqparse, Resource
from marshmallow import Schema, fields
from sqlalchemy import and_, func
from sqlalchemy.orm import aliased
from sunscrapersapi.models import db, Fx, Currency as CurrencyModel
from sunscrapersapi.resources.currency import CurrencySchema


class FxSchema(Schema):
    date = fields.DateTime(required=True)
    base_currency = fields.Nested(CurrencySchema)
    target_currency = fields.Nested(CurrencySchema)
    value = fields.Float(required=False)


class FX(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('page', type=int, required=False)
    parser.add_argument('per_page', type=int, required=False)

    def get(self, base, target):
        args = self.parser.parse_args()
        if 'page' not in args:
            args['page'] = current_app.config['PAGINATION_START_PAGE']
        if 'per_page' not in args:
            args['per_page'] = current_app.config['PAGINATION_PER_PAGE']

        target_cur_join = aliased(CurrencyModel)
        fx = Fx.query.\
            filter(CurrencyModel.name == base).\
            filter(target_cur_join.name == target).\
            join(Fx.base_currency).\
            join(target_cur_join, Fx.target_currency).\
            order_by(Fx.date.desc()).\
            paginate(args['page'], args['per_page'], error_out=True)
        schema = FxSchema(many=True)
        return {
            "_links": {
                "self": request.url
            },
            "result": schema.dump(fx.items),
            "total": fx.total
        }


class FXAll(Resource):
    def get(self, base):
        target_cur_join = aliased(CurrencyModel)

        subq = db.session.query(Fx.target_currency_id, func.max(Fx.date).label('maxdate')).\
            join(Fx.base_currency).\
            join(target_cur_join, Fx.target_currency).\
            group_by(Fx.target_currency_id).\
            subquery('q')

        fx = db.session.query(Fx).join(
            subq,
            and_(
                Fx.target_currency_id == subq.c.target_currency_id,
                Fx.date == subq.c.maxdate
            )
        ).join(Fx.base_currency).filter(CurrencyModel.name == base).all()
        schema = FxSchema(many=True)
        return {
            "result": schema.dump(fx),
            "total": len(fx)
        }
