from flask_restful import Resource
from sunscrapersapi.models import Currency as CurrencyModel
from marshmallow import Schema, fields


class CurrencySchema(Schema):
    name = fields.Str(required=True)


class Currency(Resource):
    def get(self):
        cur = CurrencyModel.query.all()
        schema = CurrencySchema(many=True)
        return {"result":schema.dump(cur), "total": len(cur)}
