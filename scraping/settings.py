BOT_NAME = 'sunscrapersapibot'

SPIDER_MODULES = ['scraping.spiders']
NEWSPIDER_MODULE = 'scraping.spiders'

# Obey robots.txt rules
ROBOTSTXT_OBEY = True

ITEM_PIPELINES = {
    'scraping.pipelines.DbWriterPipeline': 300
}
