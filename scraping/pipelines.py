from sunscrapersapi import create_app
from sunscrapersapi.models import Currency, Fx, db


class DbWriterPipeline(object):
    app = create_app()
    ctx = app.app_context()
    ctx.push()

    def _get_or_create(self, currency_name):
        currency = Currency.query.filter(Currency.name == currency_name).first()
        if not currency:
            print("Creating new currency: {}".format(currency_name))
            currency = Currency(name=currency_name)
            db.session.add(currency)
        return currency

    def close_spider(self, spider):
        db.session.commit()

    def process_item(self, item, spider):
        base_currency = self._get_or_create(item['base_currency'])
        target_currency = self._get_or_create(item['target_currency'])

        if not Fx.query. \
                filter(Fx.base_currency == base_currency,
                       Fx.target_currency == target_currency,
                       Fx.date == item['date']). \
                first():
            del (item['target_currency'])
            del (item['base_currency'])
            new_fx = Fx(**item)
            new_fx.base_currency = base_currency
            new_fx.target_currency = target_currency
            db.session.add(new_fx)
        else:
            print('Entry already exist.')
