from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class ECBSpider(CrawlSpider):
    name = 'ecbspider'
    start_urls = ['https://www.ecb.europa.eu/home/html/rss.en.html']

    rules = (
        Rule(LinkExtractor(allow=('/rss/fxref')), callback='parse_item'),
    )

    def parse_item(self, response):
        response.selector.remove_namespaces()
        for item in response.xpath("//item"):
            yield {
                'date': item.xpath("date/text()").get(),
                'base_currency': item.xpath("statistics/exchangeRate/baseCurrency/text()").get(),
                'target_currency': item.xpath("statistics/exchangeRate/targetCurrency/text()").get(),
                'value': float(item.xpath("statistics/exchangeRate/value/text()").get())
            }
